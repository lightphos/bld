package processor

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/lightphos/bld/builder"
)

type execCmd struct {
	cmd  string
	envs []builder.KVPair
}

func out(o interface{}, err error) {
	fmt.Printf("%s\n", o)
	if err != nil {
		log.Fatal("Oops! ", err)
	}
}

func replaceNChar(pos int, input string, newChar byte) string {
	if len(input) == 0 {
		return input
	}

	runes := []rune(input)
	runes[pos] = rune(newChar)

	return string(runes)
}

func curlSpecial(s string) []string {
	st := strings.TrimSpace(s)
	if strings.HasPrefix(st, "curl") {
		st = strings.ReplaceAll(st, "'", "")
		st = strings.ReplaceAll(st, ": ", ":")
		st = strings.ReplaceAll(st, ", ", ",")
		st = strings.ReplaceAll(st, "Basic ", "Basic~")
		st = strings.ReplaceAll(st, "Bearer ", "Bearer~")
		// fmt.Printf("-------> %v\n", st)
		fs := make([]string, 0)
		for _, ss := range strings.Split(st, " ") {
			if len(ss) > 0 {
				if strings.HasPrefix(ss, "\"") && strings.HasSuffix(ss, "\"") {
					ss = replaceNChar(0, ss, ' ')
					ss = replaceNChar(len(ss)-1, ss, ' ')
					ss = strings.TrimSpace(ss)
				}
				ss = strings.ReplaceAll(ss, "~", " ")
				fs = append(fs, ss)
			}
		}
		return fs
	}
	return strings.Split(s, " ")
}

// processCommand command
func processCommand(ec execCmd) {
	s := strings.TrimSpace(ec.cmd)
	if len(s) > 0 {
		suppressError := false
		if strings.HasPrefix(s, "-") {
			suppressError = true
			s = s[1:]
		}

		fmt.Println(">", s) // add verbose?

		var cmd *exec.Cmd
		if strings.Contains(s, "|") || strings.HasSuffix(s, ".sh") { // >< ?
			cmd = exec.Command("bash", "-c", s)
		} else {
			sa := curlSpecial(s)
			// for si, ss := range sa {
			// 	fmt.Printf("%d [%v]\n", si, ss)
			// }
			cmd = exec.Command(sa[0], sa[1:]...)
		}

		cmd.Env = os.Environ()
		for _, e := range ec.envs {
			cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", e.K, e.V))
		}

		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Run()
		if err != nil {
			if !suppressError {
				log.Fatalf("Failed with %s\n", err)
			} else {
				log.Printf("Failed with %s, continue...\n", err)
			}
		}
	}
}

func extra(extraCommands []string, process func(execCmd)) {
	for _, ex := range extraCommands {
		ex = strings.TrimSpace(ex)
		if strings.HasPrefix(ex, "cd ") {
			os.Chdir(strings.TrimPrefix(ex, "cd "))
		} else {
			process(execCmd{cmd: ex})
		}
	}
}

func checkTag(t string) {
	if builder.GetBlock(t) == nil {
		fmt.Printf("bld: tag [%s] not defined\n", t)
		os.Exit(1)
	}
}

func processParents(s builder.Script, b builder.Block, process func(execCmd)) {
	for _, p := range b.ParentTags {
		checkTag(p)
		runScript(s, p, process)
	}
}

func processCommands(s builder.Script, b builder.Block, process func(execCmd)) {
	for _, c := range b.Commands {
		extraCommands := strings.Split(c, ";")
		if len(extraCommands) > 1 {
			extra(extraCommands, process)
		} else {
			process(execCmd{cmd: c, envs: b.Envs})
		}
	}
}

// RunScript process the script
func runScript(s builder.Script, target string, process func(execCmd)) {
	if process == nil {
		process = processCommand
	}

	if target != "" {
		blk := builder.GetBlock(target)
		processParents(s, *blk, process)
		processCommands(s, *blk, process)
	}

}

// Execute script
func Execute(s builder.Script, target []string) {
	for _, t := range target {
		checkTag(t)
		runScript(s, t, processCommand)
	}
	if len(target) < 1 {
		for _, b := range s.Blocks {
			processParents(s, b, processCommand)
			processCommands(s, b, processCommand)
		}
	}
}
