package processor

import (
	"reflect"
	"testing"

	"gitlab.com/lightphos/bld/builder"
)

var processed []execCmd

func prcCmd(ec execCmd) {
	processed = append(processed, ec)
}

func TestRunScript(t *testing.T) {
	type args struct {
		s      builder.Script
		target string
	}

	v := builder.KVPair{K: "avar", V: "builder"}

	// avar=subs
	// @pa
	// ls {avar}
	// @tag pa
	// ls tag
	parentBlock := builder.Block{Tag: "pa", Commands: []string{"ls {avar}"}}
	b := builder.Block{ParentTags: []string{"pa"}, Tag: "tag", Commands: []string{"ls processor"}}
	ba := []builder.Block{parentBlock, b}
	s := builder.Script{Blocks: ba, Vars: []builder.KVPair{v}}
	builder.AddBlock("pa", &parentBlock)
	builder.AddBlock("tag", &b)
	s.ReplaceVars()

	tests := []struct {
		name string
		args args
		want []execCmd // <- execCmd
	}{
		{"test tag", args{s, "tag"}, []execCmd{{cmd: "ls builder"}, {cmd: "ls processor"}}},
		{"test parent", args{s, "pa"}, []execCmd{{cmd: "ls builder"}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runScript(tt.args.s, tt.args.target, prcCmd)
			if !reflect.DeepEqual(processed, tt.want) {
				t.Errorf("runScript = %v, got %v, want %v", tt.name, processed, tt.want)
			}

			processed = nil
		})
	}

	t.Log("Result")
	t.Log(processed)
}
