package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/lightphos/bld/builder"
	"gitlab.com/lightphos/bld/processor"
)

const ver = "0.0.7"
const usage = `
usage: bld [-v] [-f {buildfile}]
  defaults to build.bld file
  -v version
  -s sub=act
  -f buildfile.bld
      buildfile:
    	var=avar

        @tag parent-tag
        commmands to run {var}

`

func main() {
	help := flag.Bool("h", false, "Show help")
	versionFlag := flag.Bool("v", false, "Show version")
	filen := flag.String("f", "build.bld", "Specify build file")
	flag.Parse()

	if *help {
		fmt.Printf("%s", usage)
		os.Exit(0)
	}
	if *versionFlag {
		fmt.Println(ver)
		os.Exit(0)
	}

	file, err := os.Open(*filen)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	script := builder.Parse(file)
	processor.Execute(script, flag.Args())

}
