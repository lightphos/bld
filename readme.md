## BLD

### Building bld
go build -o bld .

### Using bld

Create a file called build.bld

Prefix targets with @
All lines under the target will be processed

Create variables by using assignment with =
Use these variables enclosed in {}
NOTE: variable need to be defined before any target.

Reference any other targets in the line, which will be run before the target.

Comment use #

For envrionment variables use

ENV > value 

after the target to apply to the command block in this target.

Example:
A build file containing
```
@p
pwd; uname -a

@ls p
ls -la

@env 
ABC > "abcd4"

```

Running
`bld ls` 

will first call the target p, then run ls
```
> uname -a
MINGW64_NT-10.0 W5171054 2.11.2(0.329/5/3) 2018-11-10 14:38 x86_64 Msys

> ls -la
total 4495
drwxr-xr-x 1 C60516A 1049089       0 Nov 13 10:23 .
drwxr-xr-x 1 C60516A 1049089       0 Oct  1 14:52 ..
drwxr-xr-x 1 C60516A 1049089       0 Nov 13 10:23 .git
-rw-r--r-- 1 C60516A 1049089      31 Oct  1 14:52 .gitignore
-rw-r--r-- 1 C60516A 1049089     515 Oct  1 14:52 .gitlab-ci.yml
-rwxr-xr-x 1 C60516A 1049089 2222592 Nov 13 10:22 bld
-rw-r--r-- 1 C60516A 1049089     428 Nov 13 10:23 build
drwxr-xr-x 1 C60516A 1049089       0 Nov 13 10:23 builder
-rw-r--r-- 1 C60516A 1049089     594 Oct  1 14:52 Dockerfile
-rw-r--r-- 1 C60516A 1049089      41 Oct  1 14:53 go.mod
-rw-r--r-- 1 C60516A 1049089    1058 Oct  1 14:52 LICENSE
-rw-r--r-- 1 C60516A 1049089 2357984 Oct  1 14:52 main
-rw-r--r-- 1 C60516A 1049089     368 Oct 15 11:41 main.go
drwxr-xr-x 1 C60516A 1049089       0 Oct  1 15:01 processor
-rw-r--r-- 1 C60516A 1049089     296 Nov 13 10:27 readme.md
```
