package builder

import (
	"bytes"
	"fmt"
	"io"
	"reflect"
	"testing"
)

const testScript string = `
var=sr
// comment
@test 
echo test
`

const testScriptWithParent string = `
var=sr
// comment
@par
ls

@test par
echo {var}
`

func scriptsEqual(got Script, want Script) bool {
	if !reflect.DeepEqual(got.Vars, want.Vars) {
		fmt.Printf("Vars different got [%v], want [%v]", got.Vars, want.Vars)
		return false
	}

	gotb := fmt.Sprint(got.Blocks)
	wantb := fmt.Sprint(want.Blocks)
	if gotb != wantb {
		fmt.Printf("Blocks different got %v, want %v", gotb, wantb)
		return false
	}

	return true
}

func TestParsing(t *testing.T) {
	rd := bytes.NewBufferString(testScript)
	rdp := bytes.NewBufferString(testScriptWithParent)

	type args struct {
		reader io.Reader
	}

	commands := []string{"echo test"}
	vars := []KVPair{{"var", "sr"}}
	blocks := []Block{{ParentTags: []string{}, Tag: "test", Commands: commands}}
	expectScript := Script{Vars: vars, Blocks: blocks}
	blocksWithParent := []Block{
		{ParentTags: []string{}, Tag: "par", Commands: []string{"ls "}},
		{ParentTags: []string{"par"}, Tag: "test", Commands: []string{"echo sr"}},
	}
	expectScriptWithParent := Script{Vars: vars, Blocks: blocksWithParent}

	tests := []struct {
		name string
		args args
		want Script
	}{
		{"script", args{rd}, expectScript},
		{"scriptParent", args{rdp}, expectScriptWithParent},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Parse(tt.args.reader); !scriptsEqual(got, tt.want) {
				t.Errorf("Parse() = %v, want %v", got, tt.want)
			}
		})
	}
}
