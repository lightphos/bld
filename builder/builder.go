package builder

import (
	"bufio"
	"io"
	"strings"
)

const commentIndication = "#"
const continuation = `\`

type KVPair struct {
	K string
	V string
}

type Block struct {
	ParentTags []string
	Tag        string
	Commands   []string
	Envs       []KVPair
}

type Script struct {
	Vars         []KVPair
	Blocks       []Block
	currentBlock *Block
}

var tagMap map[string]*Block = make(map[string]*Block)

func GetBlock(tag string) *Block {
	return tagMap[tag]
}

func AddBlock(tag string, block *Block) {
	tagMap[tag] = block
}

func (script *Script) ParseVars(line string) bool {
	if !strings.Contains(line, "=") {
		return false
	}

	nv := strings.Split(line, "=")
	v := KVPair{nv[0], nv[1]}
	script.Vars = append(script.Vars, v)
	return true
}

func (script *Script) parseEnvs(line string) bool {
	if !strings.Contains(line, ">") {
		return false
	}

	if script.currentBlock == nil {
		return false
	}

	nv := strings.Split(line, ">")
	v := KVPair{strings.TrimSpace(nv[0]), strings.TrimSpace(nv[1])}
	script.currentBlock.Envs = append(script.currentBlock.Envs, v)

	return true
}

func (script *Script) parseTag(line string) bool {
	if !strings.HasPrefix(line, "@") {
		return false
	}

	if script.currentBlock != nil {
		script.Blocks = append(script.Blocks, *script.currentBlock) // save away the current block
	}

	script.currentBlock = &Block{} // start new block
	tag := strings.TrimSpace(strings.TrimPrefix(line, "@"))
	parents := strings.Split(tag, " ")
	script.currentBlock.Tag = parents[0]
	AddBlock(script.currentBlock.Tag, script.currentBlock)
	if len(parents) > 1 {
		script.currentBlock.ParentTags = parents[1:]
	}

	return true
}

func (script *Script) parseCommands(line string) {
	if script.currentBlock != nil {
		if script.parseEnvs(line) {
			return
		}
		cmds := &script.currentBlock.Commands
		*cmds = append(*cmds, line) // add to the commands
	}
}

func (script *Script) parseBlocks() {
	if script.currentBlock != nil {
		script.Blocks = append(script.Blocks, *script.currentBlock)
	}
}

func getPlaceholder(vs []KVPair, ph string) string {
	for _, nv := range vs {
		if nv.K == ph {
			return nv.V
		}
	}
	return ""
}

// ReplaceVars replace
func (script *Script) ReplaceVars() {
	for _, b := range script.Blocks {
		for ix, c := range b.Commands {

			if strings.Contains(c, "{") && strings.Contains(c, "}") {
				sa := strings.Split(c, "{")
				cmdp := sa[0]
				cmdr := sa[1]
				phl := strings.Split(cmdr, "}")
				ph := getPlaceholder(script.Vars, phl[0])
				if len(ph) > 0 {
					b.Commands[ix] = cmdp + ph + phl[1]
				}
			}
		}
	}

}

func isComment(line string) bool {
	return strings.HasPrefix(strings.TrimSpace(line), commentIndication)
}

func hasComment(line string) string {
	l := strings.Split(line, commentIndication)
	if len(l) > 0 {
		return l[0]
	}

	return line
}

func hasContinuation(line string) (string, bool) {
	l := strings.TrimSpace(line)
	if strings.HasSuffix(l, continuation) {
		return l[:len(l)-1], true
	}

	return line, false
}

// Parse the build Script
func Parse(reader io.Reader) Script {

	rd := bufio.NewReader(reader)

	script := &Script{}
	scanner := bufio.NewScanner(rd)
	haveTag := false
	var prevLine string
	for scanner.Scan() {
		line := scanner.Text()
		if isComment(line) {
			continue
		}

		line = hasComment(line)

		newLine, ok := hasContinuation(line)
		if ok {
			prevLine += newLine
			continue
		}

		if len(prevLine) > 0 { // any continuation
			line = prevLine + line
			prevLine = ""
		}

		if !haveTag && script.ParseVars(line) { // only parse vars before any tags, once we have a tag ignore
			continue
		}

		if script.parseTag(line) {
			haveTag = true
			continue
		}

		script.parseCommands(line)
	}

	script.parseBlocks()
	script.ReplaceVars()

	return *script
}
